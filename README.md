# Whiny Stinky Bot

## General Commands
### $help
Gets a list of all commands.

### $source
Links to this page to view the code.

### $crypto <symbols>
Uses the [CoinMarketCap](https://coinmarketcap.com/) API to get the latest data on crypto.

Returns the price, 24h percent change, and 7d percent change.

Example `$crypto eth` or `$crypto dot,ada,btc`

### $gif <search term>
Uses [Tenor](https://tenor.com/), the same gif search engine that Discord uses.

Returns the top 20 gifs based on the search term and randomly chooses 1.

Example `$gif cats`

### $roll20
Randomly selects a number between 1-20

### $ask
Answers yes or no questions with 100% accuracy. ;)

### $play
Play an audio file saved on the server using the files name (with or without extension)

### $join
Have the bot join the current voice channel

### $dismiss
Kick the bot from it's current voice channel

## Admin Commands
### $reload
Reload the replies json file back into memory.
This is done after adding or updating replies text.

## Triggers
### Member Joins
The bot will give introductions, point to the info channel, and show how to use it's commands.

## New Functionality!
Message Awecelot#9986 to suggest new commands or features!

## Development
### APIs
`DISCORD_TOKEN=.. TENOR_TOKEN=.. CRYPTO_TOKEN=.. ..path/ws-bot`
- DISCORD_TOKEN: [Discord Developer Portal](https://discord.com/developers/applications) Bot token
- TENOR_TOKEN: [Tenor API](https://tenor.com/gifapi/documentation) api token
- CRYPTO_TOKEN: [CoinMarketCap API](https://coinmarketcap.com/api/) api token

### Dependencies
These dependencies are only to make playing audio in the voice channels work.

#### Windows
Add both bins to system environment variable PATH
- FFMPEG: [FFMPEG Download](https://www.ffmpeg.org/download.html)
- VS2019 > Desktop development with C++ > C++ Clang tools for Windows

#### Linux
- apt install build-essential autoconf automake libtool m4
- apt install libopus-dev
- apt install ffmpeg
- apt install youtube-dl

### Raspberry Pi Cross Compilation From Windows
1. Install cross in project
    - cargo install cross
    - [Cross Github](https://github.com/rust-embedded/cross)
2. Add latest openssl crate to Cargo.toml dependencies
    - `openssl = {version = "latest", features = ["vendored"]}`
3. Turn on windows features
    - Windows Subsystem for Linux
    - Virtual Machine Platform
4. Make sure virtualization is on in bios
5. Install wsl_update_x64.msi downloaded from [Docker](https://docs.docker.com/docker-for-windows/install/)
6. Open admin powershell `wsl --set-default-version 2`
7. Install [Docker](https://docs.docker.com/docker-for-windows/install/) for windows
8. Run docker
9. Install Rust target platform for the current Rust toolchain
    - `rustup target add armv7-unknown-linux-gnueabihf`
    - `rustup target add aarch64-unknown-linux-gnu` (for 64bit Pi)
10. compile with cross
    - `cross build --target armv7-unknown-linux-gnueabihf`
    - `cross build --target aarch64-unknown-linux-gnu` (for 64bit Pi)
