use log::LevelFilter;
use log4rs::{
    append::file::FileAppender,
    config::{Appender, Config, Root},
    encode::pattern::PatternEncoder,
};
use my::{
    commands::{
        cmc::crypto::*,
        core::{ask::*, dismiss::*, help::*, join::*, play::*, reload::*, roll20::*, source::*},
        tenor::gif::*,
    },
    handler::handler::Handler,
    helpers::{create_file, file_path_exists, get_file_path, map_apis, map_configuration},
    log::log_error,
    models::core::{ApiStore, ConfigStore},
};
use serenity::{
    client::{Client, Context},
    framework::{
        standard::{
            macros::{group, hook},
            CommandError,
        },
        StandardFramework,
    },
    model::channel::Message,
    prelude::*,
};
use songbird::SerenityInit;
use std::{env, sync::Arc};

mod my;

#[group]
#[only_in(guilds)]
#[commands(help, source, crypto, gif, roll20, ask, play, join, dismiss)]
struct General;

#[group]
#[allowed_roles(admin)]
#[commands(reload)]
struct Admin;

#[tokio::main]
async fn main() {
    // grab discord token from the environment
    let discord_token =
        env::var("DISCORD_TOKEN").expect("discord token not found in the environment");

    init_logging();

    // setup serenity discord client
    let framework = StandardFramework::new()
        .configure(|c| c.prefix("$"))
        .after(after_hook)
        .group(&ADMIN_GROUP)
        .group(&GENERAL_GROUP);
    let mut client = Client::builder(&discord_token)
        .framework(framework)
        .register_songbird()
        .event_handler(Handler)
        .await
        .expect("error creating serenity client");

    // store replies json data onto the client
    {
        let config_map = map_configuration();
        let api_map = map_apis();
        let mut data = client.data.write().await;
        data.insert::<ConfigStore>(Arc::new(Mutex::new(config_map)));
        data.insert::<ApiStore>(Arc::new(Mutex::new(api_map)));
    }

    // start serenity client
    if let Err(e) = client.start().await {
        println!("error starting serenity client: {:?}", e);
    }
}

/// general error logging for failed commands
#[hook]
async fn after_hook(_: &Context, _: &Message, cmd_name: &str, error: Result<(), CommandError>) {
    if let Err(why) = error {
        log_error(&format!("Command '{}' returned error {:?}", cmd_name, why));
    }
}

/// Init error logging
fn init_logging() {
    if !file_path_exists("hidden-assets/output.log") {
        create_file("hidden-assets/output.log");
    }
    let log_path = get_file_path("hidden-assets/output.log");
    let logfile = FileAppender::builder()
        .encoder(Box::new(PatternEncoder::new(
            "{d} [{l}] line: {L}\n{m}\n\n",
        )))
        .build(log_path)
        .expect("error building file appender");
    let config = Config::builder()
        .appender(Appender::builder().build("logfile", Box::new(logfile)))
        .build(Root::builder().appender("logfile").build(LevelFilter::Warn))
        .expect("error building log configuration");
    log4rs::init_config(config).expect("error initializing logging configuration");
}
