use log::{error, warn};

pub fn log_warn(content: &str) {
    warn!("Warning: {}", content);
}

pub fn log_error(content: &str) {
    println!("Error: {}", content);
    error!("Error: {}", content);
}
