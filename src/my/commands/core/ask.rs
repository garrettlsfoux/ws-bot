use crate::my::helpers::send_rand_reply;
use serenity::{
    framework::standard::{macros::command, Args, CommandResult},
    model::prelude::*,
    prelude::*,
};

/// $ask
#[command]
async fn ask(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let question = args.message().trim();

    if question.len() > 0 && question.contains("?") {
        send_rand_reply(&ctx, &msg, "ASK").await;
    } else {
        send_rand_reply(&ctx, &msg, "ASK_ERROR").await;
    }

    Ok(())
}
