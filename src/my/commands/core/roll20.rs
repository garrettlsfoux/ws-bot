use crate::my::helpers::send_rand_reply;
use serenity::{
    framework::standard::{macros::command, CommandResult},
    model::prelude::*,
    prelude::*,
};

/// $roll20
#[command]
async fn roll20(ctx: &Context, msg: &Message) -> CommandResult {
    send_rand_reply(&ctx, &msg, "ROLL20").await;
    Ok(())
}
