use crate::my::log::log_error;
use serenity::{
    framework::standard::Args,
    framework::standard::{macros::command, CommandResult},
    model::prelude::*,
    prelude::*,
};

#[command]
async fn dismiss(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    if let Err(why) = msg.delete(&ctx.http).await {
        log_error(&format!("Failed to delete message. {:?}", why));
    };

    let guild = msg.guild(&ctx.cache).await.unwrap();
    let guild_id = guild.id;

    let manager = songbird::get(ctx)
        .await
        .expect("Songbird Voice client placed in at initialisation.")
        .clone();

    if let Some(_) = manager.get(guild_id) {
        if let Err(why) = manager.remove(guild_id).await {
            log_error(&format!("Failed to leave voice channel. {:?}", why));
        }
    }

    Ok(())
}
