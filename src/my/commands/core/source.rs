use crate::my::helpers::send_single_reply;
use serenity::{
    framework::standard::{macros::command, CommandResult},
    model::prelude::*,
    prelude::*,
};

/// $source
#[command]
async fn source(ctx: &Context, msg: &Message) -> CommandResult {
    send_single_reply(&ctx, &msg, "SOURCE").await;
    Ok(())
}
