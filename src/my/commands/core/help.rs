use crate::my::helpers::send_msg;
use serenity::{
    framework::standard::{macros::command, CommandResult},
    model::prelude::*,
    prelude::*,
};

/// $help
#[command]
async fn help(ctx: &Context, msg: &Message) -> CommandResult {
    send_msg(ctx, msg, "HELP").await;
    Ok(())
}
