use crate::my::log::log_error;
use serenity::{
    framework::standard::Args,
    framework::standard::{macros::command, CommandResult},
    model::prelude::*,
    prelude::*,
};

#[command]
async fn join(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    if let Err(why) = msg.delete(&ctx.http).await {
        log_error(&format!("Failed to delete message. {:?}", why));
    };

    let guild = msg.guild(&ctx.cache).await.unwrap();
    let guild_id = guild.id;

    let channel_id = guild
        .voice_states
        .get(&msg.author.id)
        .and_then(|voice_state| voice_state.channel_id);

    let connect_to = match channel_id {
        Some(channel) => channel,
        None => return Ok(()),
    };

    let manager = songbird::get(ctx)
        .await
        .expect("Songbird Voice client placed in at initialisation.")
        .clone();

    // join the voice channel of the member who sent the command
    let (_, join_result) = manager.join(guild_id, connect_to).await;

    if let Err(why) = join_result {
        log_error(&format!("Failed to join channel: {:?}", why));
    }

    Ok(())
}
