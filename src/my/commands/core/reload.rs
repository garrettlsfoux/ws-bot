use crate::my::{
    helpers::{set_apis, set_replies},
    log::log_error,
};
use serenity::{
    framework::standard::{macros::command, Args, CommandResult},
    model::prelude::*,
    prelude::*,
};

/// $reload
#[command]
async fn reload(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    if let Err(why) = msg.delete(&ctx.http).await {
        log_error(&format!("Failed to delete message. {:?}", why));
    };

    set_replies(ctx).await;
    set_apis(ctx).await;

    Ok(())
}
