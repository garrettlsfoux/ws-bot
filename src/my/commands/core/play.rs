use crate::my::{
    helpers::{file_path_exists, get_file_names, get_file_path, send_say},
    log::log_error,
};
use serenity::{
    async_trait,
    framework::standard::Args,
    framework::standard::{macros::command, CommandResult},
    model::id::GuildId,
    model::prelude::*,
    prelude::*,
};
use songbird::{Event, EventContext, EventHandler as VoiceEventHandler, Songbird, TrackEvent};
use std::sync::Arc;

pub struct AudioEndNotifier {
    pub manager: Arc<Songbird>,
    pub guild_id: GuildId,
}

#[async_trait]
impl VoiceEventHandler for AudioEndNotifier {
    async fn act(&self, ctx: &EventContext<'_>) -> Option<Event> {
        if let EventContext::Track(_track_list) = ctx {
            let has_handler = self.manager.get(self.guild_id).is_some();

            if has_handler {
                if let Err(why) = self.manager.remove(self.guild_id).await {
                    log_error(&format!("Failed to leave voice channel.{:?}", why));
                }
            } else {
                log_error("Not in a voice channel");
            }
        }
        None
    }
}

#[command]
async fn play(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    // check if the bot should play a single audio and leave
    let mut is_single = false;

    // grab the file name from the args
    let mut file_name = match args.single::<String>() {
        Ok(file_name) => {
            let file_name_lwr = file_name.to_lowercase();

            if file_name == "--help" {
                if let Some(audio_files) = get_file_names("audio") {
                    send_say(ctx, msg, &audio_files.join(", ")).await;
                }

                return Ok(());
            } else {
                if let Err(why) = msg.delete(&ctx.http).await {
                    log_error(&format!("Failed to delete message. {:?}", why));
                };

                file_name_lwr
            }
        }
        Err(_) => return Ok(()),
    };

    if !file_name.contains(".") {
        file_name = file_name + ".mp3";
    }

    // check if the file exists in the audio folder
    if !file_path_exists(&format!("audio/{}", file_name)) {
        return Ok(());
    }

    let guild = msg.guild(&ctx.cache).await.unwrap();
    let guild_id = guild.id;

    let manager = songbird::get(ctx)
        .await
        .expect("Songbird Voice client placed in at initialisation.")
        .clone();

    // join the voice channel of the member who sent the command
    let handler_lock = match manager.get(guild_id) {
        Some(handler_lock) => {
            // set to false because the bot was already in the voice channel
            handler_lock
        }
        None => {
            // set to true if the bot wasn't already in a voice channel
            is_single = true;

            let channel_id = guild
                .voice_states
                .get(&msg.author.id)
                .and_then(|voice_state| voice_state.channel_id);

            let connect_to = match channel_id {
                Some(channel) => channel,
                None => {
                    log_error("play command called with no channel_id found.");

                    return Ok(());
                }
            };

            let (handler_lock, join_result) = manager.join(guild_id, connect_to).await;
            if let Err(why) = join_result {
                log_error(&format!("Failed to join channel: {:?}", why));

                return Ok(());
            }

            handler_lock
        }
    };

    let mut handler = handler_lock.lock().await;

    let path = get_file_path(&format!("audio/{}", file_name));
    let source = match songbird::ffmpeg(path).await {
        Ok(source) => source,
        Err(why) => {
            log_error(&format!("Error starting audio source: {:?}", why));

            return Ok(());
        }
    };

    // play the audio file
    let audio = handler.play_source(source);

    if is_single {
        // add an event to leave the voice channel after the audio file has ended
        let _event = audio.add_event(
            Event::Track(TrackEvent::End),
            AudioEndNotifier { manager, guild_id },
        );
    }

    Ok(())
}
