use crate::my::helpers::send_rand_gif;
use serenity::{
    framework::standard::{macros::command, Args, CommandResult},
    model::prelude::*,
    prelude::*,
};

/// $gif
#[command]
async fn gif(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let search = args.message().trim();
    send_rand_gif(&ctx, &msg, &search).await;
    Ok(())
}
