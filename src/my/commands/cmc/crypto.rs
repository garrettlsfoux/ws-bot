use crate::my::{
    helpers::{get_api_host, send_say},
    log::log_error,
    models::cmc::CmcResponse,
};
use serenity::{
    framework::standard::{macros::command, Args, CommandResult},
    model::prelude::*,
    prelude::*,
};

/// $crypto
#[command]
async fn crypto(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    match get_api_host(ctx, "cmc", "awecelot").await {
        Some(host) => {
            let symbols = args.message().trim().replace(" ", "");

            if symbols.is_empty() {
                send_say(
                    &ctx,
                    &msg,
                    ":face_palm: Maybe throw in some symbols with that. ex: 'btc, eth'",
                )
                .await;
                return Ok(());
            }

            let client = reqwest::Client::new();
            match client
                .get(format!(
                    "{}/cryptocurrency/quotes/latest?symbol={}",
                    host.target_url, symbols
                ))
                .header("X-CMC_PRO_API_KEY", host.token)
                .send()
                .await
            {
                Ok(r) => match r.json::<CmcResponse>().await {
                    Ok(response_parsed) => match response_parsed.data {
                        Some(crypto_data) => {
                            if &crypto_data.len() > &0 {
                                let mut prices = String::new();
                                for (_, crypto) in &crypto_data {
                                    let quote = &crypto.quote["USD"];
                                    prices.push_str(&format!(
                                "```diff\n{} ${:.2}\n{}{:.2}% 24h\n{}{:.2}% 7d\n{}{:.2}% 30d```",
                                crypto.symbol,
                                quote.price.abs(),
                                if quote.percent_change_24h.is_sign_positive() { "+" } else { "-" },
                                quote.percent_change_24h.abs(),
                                if quote.percent_change_7d.is_sign_positive() { "+" } else { "-" },
                                quote.percent_change_7d.abs(),
                                if quote.percent_change_30d.is_sign_positive() { "+" } else { "-" },
                                quote.percent_change_30d.abs()));
                                }
                                send_say(&ctx, &msg, &prices).await;
                            } else {
                                send_say(
                                    &ctx,
                                    &msg,
                                    ":person_shrugging: Crypto found, but no data on it.",
                                )
                                .await;
                            }
                        }
                        None => {
                            send_say(
                                &ctx,
                                &msg,
                                ":face_palm: No crypto found. ex: $crypto btc, eth",
                            )
                            .await;
                        }
                    },
                    Err(why) => log_error(&format!("Error parsing crypto response. {:?}", why)),
                },
                Err(why) => log_error(&format!("Error requesting crypto search on CMC. {:?}", why)),
            }
        }
        None => {
            return Ok(());
        }
    }
    Ok(())
}
