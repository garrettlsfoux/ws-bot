use crate::my::{
    helpers::send_msg,
    handler::uwu::uwu_trigger
};
use serenity::{
    async_trait,
    client::{Context, EventHandler},
    model::{
        channel::{Message, MessageType},
        gateway::Ready,
    },
};

use super::prisoner::prisoner_trigger;

pub struct Handler;

// event handler implementation for Discord functionality
#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, ctx: Context, msg: Message) {
        // prevent bots from talking to each other >:)
        if !msg.author.bot {
            // members joining
            if msg.kind == MessageType::MemberJoin {
                send_msg(&ctx, &msg, "WELCOME").await;
            } else {
                if !msg.content.contains("$") {
                    uwu_trigger(&ctx, &msg).await;
                    prisoner_trigger(&ctx, &msg).await;
                }
            }
        }
    }

    /// logs a statement letting us know the handler for our Discord bot is ready
    async fn ready(&self, _ctx: Context, ready: Ready) {
        println!("{} is connected", ready.user.name);
    }
}
