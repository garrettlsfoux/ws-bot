use crate::my::{
    helpers::{check_chance, get_rand_item, get_config, send_rand_gif},
    log::log_error
};
use serenity::{
    client::Context,
    model::channel::Message
};
use std::borrow::Borrow;

/// uwu trigger
pub async fn uwu_trigger(ctx: &Context, msg: &Message) {
    let message = msg.content.trim().to_lowercase();

    if message.contains("uwu") {
        match get_config(&ctx, "UWU").await {
            Some(reply) => {
                if check_chance(&reply.chance.expect("UWU reply has no chance.")) {
                    match reply.messages.borrow() {
                        Some(messages) => {
                            let reply_msg = get_rand_item(messages).to_owned();

                            send_rand_gif(&ctx, &msg, &reply_msg).await;
                        }
                        None => log_error("UWU reply has no messages."),
                    };
                }
            }
            None => log_error("UWU reply not found."),
        };
    }
}