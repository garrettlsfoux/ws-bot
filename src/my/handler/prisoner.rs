use crate::my::{
    helpers::{check_chance, check_msg, get_rand_item, get_config},
    log::log_error
};
use serenity::{
    client::Context,
    model::{channel::Message, prelude::RoleId}
};
use std::borrow::Borrow;

/// prisoner trigger
pub async fn prisoner_trigger(ctx: &Context, msg: &Message) {
    const JAIL_ROLE_ID: u64 = 1035260515464786012;
    let guild_id = msg.guild_id.expect("Guild_ID not found in prisoner_trigger.");

    match msg.author.has_role(&ctx.http, guild_id, RoleId(JAIL_ROLE_ID)).await {
        Ok(has_role) => {
            if has_role {
                match get_config(&ctx, "PRISONER").await {
                    Some(reply) => {
                        if check_chance(&reply.chance.expect("PRISONER reply has no chance.")) {
                            match reply.messages.borrow() {
                                Some(messages) => {
                                    let reply_msg = get_rand_item(messages).to_owned();
        
                                    check_msg(msg.reply(&ctx.http, reply_msg).await);
                                }
                                None => log_error("PRISONER reply has no messages."),
                            };
                        }
                    }
                    None => log_error("PRISONER reply not found."),
                };
            }
        }
        Err(why) => log_error(&format!("Error checking user role in prisoner_trigger. {:?}", why))
    }
}