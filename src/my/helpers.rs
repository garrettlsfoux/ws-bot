use crate::my::{
    log::{log_error, log_warn},
    models::{
        core::{Api, ApiStore, Host, Config, ConfigStore},
        tenor::TenorResponse,
    },
};
use rand::Rng;
use reqwest;
use serde::Deserialize;
use serenity::{client::Context, model::{channel::Message, prelude::ChannelId}, prelude::*, Result as SerenityResult};
use std::{
    borrow::Borrow, collections::HashMap, env, fs::read_dir, fs::File, io::BufReader, path::Path,
};

/// send a say to the discord server
pub async fn send_say(ctx: &Context, msg: &Message, content: &str) {
    send_say_with_channel_id(ctx, msg.channel_id.0, content).await;
}

/// send a say to the discord server
pub async fn send_say_with_channel_id(ctx: &Context, channel_id: u64, content: &str) {
    check_msg(ChannelId(channel_id).say(&ctx.http, content).await);
}

/// send a message to the discord server
pub async fn send_msg(ctx: &Context, msg: &Message, config_name: &str) {
    match get_single_reply(ctx, msg, config_name).await {
        Some(config_msg) => {
            check_msg(ChannelId(msg.channel_id.0).send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e.title(config_name);
        
                    e.description(config_msg);
        
                    e
                });
        
                m
            }).await);
        }
        None => (),
    };
}

/// send a reply to the discord server
pub async fn send_single_reply(ctx: &Context, msg: &Message, name: &str) {
    match get_single_reply(ctx, msg, name).await {
        Some(reply_msg) => {
            send_say(ctx, msg, &reply_msg).await;
        }
        None => (),
    };
}

/// send a randomly picked reply from a list to the discord server
pub async fn send_rand_reply(ctx: &Context, msg: &Message, name: &str) {
    match get_rand_message(ctx, msg, name).await {
        Some(reply_msg) => {
            send_say(ctx, msg, &reply_msg).await;
        }
        None => (),
    };
}

pub async fn get_config(ctx: &Context, name: &str) -> Option<Config> {
    let sources_lock = ctx
        .data
        .read()
        .await
        .get::<ConfigStore>()
        .cloned()
        .expect("Reply cache was installed at startup.");
    let sources = sources_lock.lock().await;

    return sources.get(name).cloned();
}

pub async fn get_api(ctx: &Context, name: &str) -> Option<Api> {
    let sources_lock = ctx
        .data
        .read()
        .await
        .get::<ApiStore>()
        .cloned()
        .expect("Api cache was installed at startup.");
    let sources = sources_lock.lock().await;

    return sources.get(name).cloned();
}

pub async fn get_api_host(ctx: &Context, api_name: &str, host_name: &str) -> Option<Host> {
    match get_api(ctx, api_name).await {
        Some(api) => match api.hosts.into_iter().find(|h| h.name == host_name) {
            Some(host) => Some(host),
            None => {
                log_error(&format!(
                    "Error: {} host not found in {} Api.",
                    host_name, api_name
                ));
                None
            }
        },
        None => {
            log_error(&format!("Error: {} Api name not found.", api_name));
            None
        }
    }
}

/// get the first message from a reply
pub async fn get_single_reply(ctx: &Context, msg: &Message, name: &str) -> Option<String> {
    match get_config(ctx, name).await {
        Some(reply) => match reply.messages.borrow() {
            Some(messages) => {
                let reply_msg = messages.first().unwrap().to_owned();
                let fmt_msg = format_message(msg, reply_msg);

                Some(fmt_msg)
            }
            None => {
                log_error(&format!("{} reply has no messages.", reply.name));
                None
            }
        },
        None => {
            log_error(&format!("{} reply not found.", name));
            None
        }
    }
}

/// get the first message from a reply
pub async fn get_rand_message(ctx: &Context, msg: &Message, name: &str) -> Option<String> {
    match get_config(ctx, name).await {
        Some(reply) => match reply.messages.borrow() {
            Some(messages) => {
                let reply_msg = get_rand_item(messages).to_owned();
                let fmt_msg = format_message(msg, reply_msg);

                Some(fmt_msg)
            }
            None => {
                log_error(&format!("{} reply has no messages.", reply.name));
                None
            }
        },
        None => {
            log_error(&format!("{} reply not found.", name));
            None
        }
    }
}

pub async fn set_replies(ctx: &Context) {
    let data_lock = ctx
        .data
        .write()
        .await
        .get_mut::<ConfigStore>()
        .cloned()
        .expect("Reply cache was installed at startup.");
    let mut data = data_lock.lock().await;

    let replies_map = map_configuration();

    for hash in replies_map.into_iter() {
        data.insert(hash.0, hash.1);
    }
}

pub async fn set_apis(ctx: &Context) {
    let data_lock = ctx
        .data
        .write()
        .await
        .get_mut::<ApiStore>()
        .cloned()
        .expect("Api cache was installed at startup.");
    let mut data = data_lock.lock().await;

    let apis_map = map_apis();

    for hash in apis_map.into_iter() {
        data.insert(hash.0, hash.1);
    }
}

pub fn map_configuration() -> HashMap<String, Config> {
    // Loading the replies ahead of time.
    let mut reply_map = HashMap::new();

    let replies: Vec<Config> = get_file_into("assets/config.json");

    for reply in replies.into_iter() {
        reply_map.insert(reply.name.clone(), reply);
    }

    reply_map
}

pub fn map_apis() -> HashMap<String, Api> {
    // Loading the apis ahead of time.
    let mut api_map = HashMap::new();

    let apis: Vec<Api> = get_file_into("hidden-assets/apis.json");

    for api in apis.into_iter() {
        api_map.insert(api.name.clone(), api);
    }

    api_map
}

pub async fn send_rand_gif(ctx: &Context, msg: &Message, content: &str) {
    match get_api_host(ctx, "tenor", "awecelot").await {
        Some(host) => {
            // call Tenor api and grab the top 20 gif results based on the word searched and
            // randomly choose one to send to the discord server
            match reqwest::get(format!(
                "{}/search?q={}&key={}",
                host.target_url, content, host.token
            ))
            .await
            {
                Ok(r) => {
                    let response_parsed: TenorResponse = r.json().await.unwrap();
                    let gif = &get_rand_item(&response_parsed.results).media[0].gif.url;

                    send_say(&ctx, &msg, gif).await;
                }
                Err(why) => log_error(&format!("error requesting gif on Tenor. {:?}", why)),
            }
        }
        None => {}
    }
}

pub fn get_file_into<T: for<'de> Deserialize<'de>>(file_name: &str) -> T {
    let path = get_file_path(file_name);
    let file = File::open(path).expect(&format!("failed to find file {}", file_name));
    let reader = BufReader::new(file);
    let result: T =
        serde_json::from_reader(reader).expect(&format!("failed to parse file {}", file_name));
    return result;
}

pub fn get_file_path(file_name: &str) -> String {
    let main_dir = get_main_dir();
    let path = format!("{}/{}", main_dir, file_name);
    if !Path::new(&path).exists() {
        log_error(&format!("{} file path not found.", path));
    }
    return format!("{}/{}", main_dir, file_name);
}

pub fn file_path_exists(file_name: &str) -> bool {
    let main_dir = get_main_dir();
    let path = format!("{}/{}", main_dir, file_name);
    return Path::new(&path).exists();
}

pub fn create_file(file_name: &str) {
    let main_dir = get_main_dir();
    let path = format!("{}/{}", main_dir, file_name);
    if let Err(why) = File::create(path) {
        log_error(&format!("Error creating file: {:?}", why));
    }
}

/// give back a random item from a given list
pub fn get_rand_item<T>(items: &Vec<T>) -> &T {
    let i = rand::thread_rng().gen_range(0..items.len());
    items.get(i).unwrap()
}

pub fn check_chance(chance: &u8) -> bool {
    let nums: Vec<u8> = (1..100).collect();
    let rand_num = get_rand_item(&nums);
    rand_num >= chance
}

pub fn get_file_names(dir: &str) -> Option<Vec<String>> {
    let main_dir = get_main_dir();
    match read_dir(&format!("{}/{}", main_dir, dir)) {
        Ok(paths) => Some(
            paths
                .filter_map(|entry| {
                    entry.ok().and_then(|e| {
                        e.path()
                            .file_stem()
                            .and_then(|n| n.to_str().map(|s| String::from(s)))
                    })
                })
                .collect::<Vec<String>>(),
        ),
        Err(why) => {
            log_error(&format!("Error reading directory {}. {:?}", dir, why));
            None
        }
    }
}

pub fn check_msg(result: SerenityResult<Message>) {
    if let Err(why) = result {
        log_error(&format!("Error sending message: {:?}", why));
    }
}

fn get_main_dir() -> String {
    return env::current_dir()
        .expect("failed to get current directory path")
        .to_str()
        .expect("failed to parse current directory path to string")
        .to_owned();
}

/// format the message created in config.json to dynamic data chosen
fn format_message(msg: &Message, contents: String) -> String {
    // skip process if no plaecholders are found
    if !contents.contains("{{") {
        return contents;
    }
    let mut formatted_contents = contents.clone();

    let split_placeholders: Vec<&str> = contents.split("{{").collect();
    // iterate through all palceholders found
    for split_placeholder in split_placeholders.iter() {
        if split_placeholder.contains("}}") {
            // grab inner placeholder
            let (placeholder, _) = split_placeholder.split_once("}}").unwrap();
            // if it's a # channel mention then target channel by id and use serenity channel mention
            if placeholder.contains("channel.mention#") {
                let (_, channel_id) = placeholder.split_once("#").unwrap();

                match channel_id.parse() {
                    Ok(id) => {
                        formatted_contents = replace_placeholder_with(
                            &formatted_contents,
                            &placeholder,
                            &serenity::model::id::ChannelId(id).mention().to_string(),
                        );
                    }
                    Err(e) => log_error(&e.to_string()),
                }
            } else if placeholder.contains("role.mention@") {
                // if it's a @ role mention then target role by id and use serenity role mention
                let (_, role_id) = placeholder.split_once("@").unwrap();

                match role_id.parse() {
                    Ok(id) => {
                        formatted_contents = replace_placeholder_with(
                            &formatted_contents,
                            &placeholder,
                            &serenity::model::id::RoleId(id).mention().to_string(),
                        );
                    }
                    Err(why) => log_error(&format!(
                        "error parsing role_id in format_message. {:?}",
                        why
                    )),
                }
            } else {
                // otherwise format standard placeholders
                match placeholder {
                    "author.name" => {
                        formatted_contents = replace_placeholder_with(
                            &formatted_contents,
                            &placeholder,
                            &msg.author.name,
                        );
                    }
                    "author.mention" => {
                        formatted_contents = replace_placeholder_with(
                            &formatted_contents,
                            &placeholder,
                            &msg.author.mention().to_string(),
                        );
                    }
                    _ => log_warn("format placeholder not implemented."),
                }
            }
        }
    }

    formatted_contents
}

/// replace placeholders in contents with provided string
fn replace_placeholder_with(content: &str, placeholder: &str, replace_with: &str) -> String {
    content.replace(&format!("{{{{{}}}}}", placeholder), replace_with)
}
