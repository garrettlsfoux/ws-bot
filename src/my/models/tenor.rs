use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Media {
    pub url: String,
}

#[derive(Debug, Deserialize)]
pub struct MediaKind {
    pub gif: Media,
}

#[derive(Debug, Deserialize)]
pub struct Gif {
    pub id: String,
    pub media: Vec<MediaKind>,
}

#[derive(Debug, Deserialize)]
pub struct TenorResponse {
    pub results: Vec<Gif>,
    pub next: String,
}
