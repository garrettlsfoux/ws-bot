use serde::Deserialize;
use serenity::prelude::*;
use std::{collections::HashMap, sync::Arc};

#[derive(Debug, Deserialize, Clone)]
pub struct Config {
    pub name: String,
    pub messages: Option<Vec<String>>,
    pub chance: Option<u8>,
    pub for_roles: Option<Vec<u64>>,
    pub to_channel: Option<u64>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Api {
    pub name: String,
    pub hosts: Vec<Host>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Host {
    pub name: String,
    pub target_url: String,
    pub token: String,
}

pub struct ConfigStore;
pub struct ApiStore;

impl TypeMapKey for ConfigStore {
    type Value = Arc<Mutex<HashMap<String, Config>>>;
}

impl TypeMapKey for ApiStore {
    type Value = Arc<Mutex<HashMap<String, Api>>>;
}
