use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Quote {
    pub price: f64,
    pub percent_change_24h: f64,
    pub percent_change_7d: f64,
    pub percent_change_30d: f64,
}

#[derive(Debug, Deserialize)]
pub struct Crypto {
    pub quote: std::collections::HashMap<String, Quote>,
    pub symbol: String,
}

#[derive(Debug, Deserialize)]
pub struct CmcResponse {
    pub data: Option<std::collections::HashMap<String, Crypto>>,
}
